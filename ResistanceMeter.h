/* 
 * File:   ResistanceMeter.h
 * Author: Antony Wu
 *
 * Created on 18 October 2021, 12:35 PM
 */

/* 

Purpose of this script is to generate a 10-bit analog voltage on pins AN0/AN1
The resultant voltage drop across a node will be AN1 - AN0
This voltage will be sent to PutTy via RS-232 cable for DEBUG purposes
This voltage will later be sent to LCD for system integration 

*/

#ifndef RESISTANCEMETER_H
#define	RESISTANCEMETER_H

#include <p18f452.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <delays.h>
#include "ConfigRegsPIC18F452.h"

#define R1 15000.0


unsigned char restistanceReading[20];


float voltage_AN2;

float measured_resistance;

// Resistance Functions
void ResistanceSetup(void);



// Generic ADC setup, channel selection, start ADC and clear interrupt flag
void ResistanceSetup(void)
{
            Delay1KTCYx(4);

            // Perform generic ADC setup 
            ADCSetup();

            // Clear ADC interrupt flag
            PIR1bits.ADIF = 0; 

            // Select Channel 2 for Voltage_divider V
            ADCON0bits.CHS0 = 0; 
            ADCON0bits.CHS1 = 1; 
            ADCON0bits.CHS2 = 0; 

            // Wait acquisition time
            Delay1KTCYx(4);

            // Start ADC 
            ADCON0bits.GO = 1;

            while(PIR1bits.ADIF == 0);

            // Fetch voltage at AN2
            voltage_AN2 = getVoltage();

            // Clear ADC interrupt flag
            PIR1bits.ADIF = 0; 
}

#endif

