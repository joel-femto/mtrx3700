/* 
 * File:   CapacitanceMeter.h
 * Author: Group 8
 *
 * Created on 18 October 2021, 5:15 PM
 */

#ifndef CAPACITANCEMETER_H
#define	CAPACITANCEMETER_H

#include <p18f452.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <delays.h>
#include "ConfigRegsPIC18F452.h"

#define R2 9000.0
#define instruct_cycle_time 0.4e-6; // 10MHz

int timer0;
unsigned char CapacitanceReading[20];

float voltage_AN4;
float measured_capacitance;
float t_charge; 


// Capacitance Functions
void CapacitorTimer0Setup(void);
int capacitorGetAnalogue(void);
void CapacitanceFinalSetup(void);
void CapacitanceBeginSetup(void);



void CapacitorTimer0Setup(void) 
{

    T0CON = 0b00000110; //TMR0 prescaler, 1:256, NOT-ON YET!
  
}


int capacitorGetAnalogue(void) {
    // Clear ADC interrupt flag
        PIR1bits.ADIF = 0; 

        // Select Channel 1 for V+ve
        //ADCON0bits.CHS0 = 1; 

        // Select Channel 4 for Voltage_divider V
        ADCON0bits.CHS0 = 0; 
        ADCON0bits.CHS1 = 0; 
        ADCON0bits.CHS2 = 1; 
      
        // Wait acquisition time
        Delay1KTCYx(4);
        
        // Start ADC 
        ADCON0bits.GO = 1;

        while(PIR1bits.ADIF == 0);

        // Fetch voltage at AN1
        //voltage_AN1 = getVoltage();
        
        // Fetch voltage at AN4
        //analogue_out = ADRESvoltage_AN4 = getVoltage();

        // Clear ADC interrupt flag
        PIR1bits.ADIF = 0; 
        
        return ADRES;
    
}

void CapacitanceBeginSetup(void)
{
     CapacitorTimer0Setup();
        
        TMR0H = 0;
        TMR0L = 0;
        timer0 = 0;
                
        //Delay1KTCYx(4);

        // Perform generic ADC setup 
        ADCSetup();

        capacitorGetAnalogue(); 
           
        // --------- Overall Voltage drop across a node --------- //
        // Wait till voltage drop across capacitor is 63.3% of Vdd
        
        TRISCbits.RC0 = 0; // Charge resistor pin 10K output
        PORTCbits.RC0 = 0; // Low voltage init
        PORTCbits.RC0 = 1; // Toggle charge pin
        
        T0CONbits.TMR0ON = 1; // Turn on TIMER0
    
    
}



void CapacitanceFinalSetup(void)
{
        // Define Timer 
        timer0 = TMR0L;
        timer0 = timer0 + 256*TMR0H;
        //timer0 = timer0 + TMR0L;
            
        // Convert to seconds
        t_charge = (0.4e-6)*(128)*(timer0);  
}


void CapacitanceClear(void)
{

        PORTCbits.RC0 = 0; // Clear charge pin
        TRISCbits.RC1 = 0; // Discharge pin output
        PORTCbits.RC1 = 0; // Discharge pin low
        // Wait while capacitor discharges
        
        while (capacitorGetAnalogue() > 0) {
        }
        
        TRISCbits.RC1 = 1; // Discharge pin input ==> High impedance
}

#endif	/* CAPACITANCEMETER_H */

