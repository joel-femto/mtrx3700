/* 
 * File:   VoltageMeter.h
 * Author: Group 8
 *
 * Header file for voltage
 */

#ifndef VOLTAGEMETER_H
#define	VOLTAGEMETER_H

#include <p18f452.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <delays.h>

#include "ConfigRegsPIC18F452.h"

#define Vdd 5.0


unsigned char voltageReading[20];

float voltage_AN1;
float voltage_AN0;

float voltageDrop; 


// Voltage Functions
void reverse(char* str, int len);
int intToStr(int x, char str[], int d);
void ftoa(float n, char* res, int afterpoint);
void ADCSetup(void);
void VoltageTimer0Setup(void);
float getVoltage(void);
void VoltageSetup(void);

// Reverses a string 'str' of length 'len'
void reverse(char* str, int len) {
    int i = 0, j = len - 1, temp;
    while (i < j) {
        temp = str[i];
        str[i] = str[j];
        str[j] = temp;
        i++;
        j--;
    }
}
  
// Converts a given integer x to string str[]. 
// d is the number of digits required in the output. 
// If d is more than the number of digits in x, 
// then 0s are added at the beginning.
int intToStr(int x, char str[], int d) {
    int i = 0;
    while (x) {
        str[i++] = (x % 10) + '0';
        x = x / 10;
    }
  
    // If number of digits required is more, then
    // add 0s at the beginning
    while (i < d)
        str[i++] = '0';
  
    reverse(str, i);
    str[i] = '\0';
    return i;
}
  

// NEED A FLOAT TO STRING FUNCTION HERE
void ftoa(float n, char* res, int afterpoint) {
    // Extract integer part
    int ipart = (int)n;
  
    // Extract floating part
    float fpart = n - (float)ipart;
  
    // convert integer part to string
    int i = intToStr(ipart, res, 0);
  
    // check for display option after point
    if (afterpoint != 0) {
        res[i] = '.'; // add dot
  
        // Get the value of fraction part upto given no.
        // of points after dot. The third parameter 
        // is needed to handle cases like 233.007
        fpart = fpart * pow(10, afterpoint);
  
        intToStr((int)fpart, res + i + 1, afterpoint);
    }
}


// ANALOGUE MODE SETUP HERE 
void ADCSetup(void) {
   
    ADCON0 = 0b01000001; // Fosc/8, turn on ADC module
    ADCON1 = 0b10001110; // Right justify, 1 analogue channel, VDD VSS references 
}


float getVoltage(void) {

    int voltageIn;
    float voltageOut;

    voltageIn = ADRES; // Right-justified ADRESH:ADRESL
        
    voltageOut = (voltageIn*Vdd)/1023.0;
    
    return voltageOut;
  
}

// Perform generic ADC setup, clear flags, set channels, and start ADC
void VoltageSetup(void) {

        // Perform generic ADC setup 
        ADCSetup();

        // Clear ADC interrupt flag
        PIR1bits.ADIF = 0; 

        // Select Channel 1 for V+ve
        ADCON0bits.CHS0 = 1; 

        // Wait acquisition time
        Delay1KTCYx(100);
        
        // Start ADC 
        ADCON0bits.GO = 1;
        
        while(PIR1bits.ADIF == 0);

        // Fetch voltage at AN1
        voltage_AN1 = getVoltage();

        // Clear ADC interrupt flag
        PIR1bits.ADIF = 0; 
        
         // Select Channel 0 for V-ve
        ADCON0bits.CHS0 = 0; 

        // Wait acquisition time
//        Delay1KTCYx(4);
        Delay1KTCYx(400);

        // Start ADC
        ADCON0bits.GO = 1;

        while(PIR1bits.ADIF == 0);

        voltage_AN0 = getVoltage();

        // Clear ADC interrupt flag
        PIR1bits.ADIF = 0;   
        
}


#endif	/* VOLTAGE_H */

