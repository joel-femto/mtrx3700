/**********************************************************
*  MP_LCD.h
***********************************************************
*  Created by: Antony Wu, 2021
***********************************************************
*  This program is for the LCD interface of the meter, it
*  initialises the 2 x 16 character LCD using
*  8-bit data interface, 5-dot x 8-dot characters.
* 
***********************************************************/

#ifndef MP_LCD_H
#define	MP_LCD_H

/***Libraries****************************************************************/
#include "ConfigRegsPIC18f452.h"    // Configure registers
#include "p18f452.h"                // Wrapper for PIC18
//#include <p18cxxx.h>               // wrapper for all PIC18 headers
#include <stdio.h>
#include <stdlib.h>

/***Function declaration*****************************************************/
void LCD_Startup(void);
void LCD_Init(void);                    // Initialise the LCD
void LCD_Ready(void);                   // Wait for the LCD to be ready
void LCD_Command(unsigned char cmd);    // Function to input commands to LCD
void LCD_String(const char *msg);       // Function to input string (multiple characters) to LCD
void LCD_Char(unsigned char data);      // Function to input data to LCD
void LCD_Clear(void);                   // Clear the LCD display
void LCD_Send(const char ch);           // Outputs to the LCD
char LCD_Address(void);                 // Gets the address of LCD cursor
void LCD_Voltage_Setup(void);       
void LCD_Resistor_Setup(void);
void LCD_Capacitance_Setup(void);
void LCD_Current_Setup(void);
void LCD_Inductance_Setup(void);

/***Pin defines**************************************************************/
#define DATA PORTD              // PORTD<0-7> for LCD data output
#define DATA_DIR TRISD          // PORTD direction register
#define RS PORTCbits.RC4        // PORTC pin 4 for register select
#define RS_DIR TRISCbits.TRISC4 // PORTC pin 4 direction
#define RW PORTCbits.RC5        // PORTC pin 5 for read write
#define RW_DIR TRISCbits.TRISC5 // PORTC pin 5 direction
#define EN PORTCbits.RC6        // PORTC pin 6 for enable
#define EN_DIR TRISCbits.TRISC6 // PORTC pin 6 direction

/***Global variables*********************************************************/
unsigned char MeterWelcome[16] = "VIRLC Meter ON!";
unsigned char SelectModeMsg[16] = "Select Mode:";
unsigned char VoltageHeader[16] = "M: Voltage";            // Strings to write to LCD
unsigned char ResistanceHeader[16] = "M: Resistance";
unsigned char CapacitorHeader[16] = "M: Capacitance";
unsigned char CurrentHeader[16] = "M: Current";
unsigned char InductanceHeader[16] = "M: Inductance";
unsigned char VoltageUnitDisp[16] = "       V (0-5V)";
unsigned char ResistanceUnitDisp[16] = "          Ohm";
unsigned char CapacitorUnitDisp[16] = "          uF";
unsigned char CurrentUnitDisp[16] = "        mA";
unsigned char InductanceUnitDisp[16] = "          uH";
unsigned char OutsideRangeDisp[16] = "INSERT    ";
char row1 = 0x80;      // Address of first row LCD
char row2 = 0xC0;      // Address of second row LCD
char messageStart = 0xC6;   // Start of V message


/***LCD functions************************************************************/

// Initialise the LCD for operation
void LCD_Init(void)
{        
    DATA = 0x00;            // Set up data pins to zero and as output
    DATA_DIR = 0x00;
    RS = 0x00;              // LCD control ports for output and clear
    RS_DIR = 0x00;
    RW = 0x00;
    RW_DIR = 0x00;
    EN = 0x00;
    EN_DIR = 0x00;
    LCD_Command(0x38);      // 8 bit mode, 5x8 characters, 2 lines
    LCD_Command(0x0C);      // Turn on display, cursor and blinking off
    LCD_Command(0x14);      // Shift cursor right
    LCD_Command(0x01);      // Clear display and move cursor home
}

// Tests if the LCD is ready for another command, and not busy
void LCD_Ready(void)
{
    unsigned char test = 0x80;       // For testing the busy flag
    DATA_DIR = 0xFF;        // Configure the LCD data pins as input
    while( test )
    {
        RS = 0;             // Select the command register
        RW = 1;             // Select read mode from LCD to MCU
        EN = 1;             // High to low pulse on enable pin to read data
        test = DATA;        // Read from LCD
        Nop();              // Small delay required to lengthen EN pulse
        Nop();
        EN = 0;             // Complete the read cycle
        test &= 0x80;       // Check bit 7 - BUSY FLAG
    }
    DATA_DIR = 0x00;        // Reset LCD data pins as output
}

// Input commands to the LCD
void LCD_Command(unsigned char cmd)
{  
    LCD_Ready();        // Wait until LCD is not busy
    RS = 0;             // Select the command register
    RW = 0;             // Select write mode from MCU to LCD
    EN = 1;             // High to low pulse on enable pin to latch data
    Nop();              // Small delay required to lengthen EN pulse
    Nop();
    DATA = cmd;         // Send the command to LCD
    Nop();              // Small delay required to lengthen EN pulse
    Nop();
    EN = 0;             
}

// Inputs a single character data to display on the LCD
void LCD_Char(unsigned char dat)
{
    char addr;
    LCD_Ready();        // Wait for not busy
    LCD_Send(dat);      // Sends character to LCD - since it cannot be displayed at the end of a row, need to reoutput it to first column
                        // MIGHT BE ABLE TO DELETE THIS PART????????????????
    LCD_Ready();        // Wait for not busy
    
    addr = LCD_Address();   // Get the cursor address
    
    // Test if the address is at the end of the first row, prevent it exceeding row boundaries
    // Using 0x11 instead of 0x10 since cursor is at the next position on right
    if( addr == 0x11 )          // Output it to the column 1 of next row
    {
        LCD_Command(0xC0);  // Read data from internal RAM
        LCD_Ready();        // Wait for LCD not busy
        LCD_Send(dat);      // Send the data
    }
}

// Call the LCD_Char function multiple times to print a string
void LCD_String(const char *msg)
{
    // Loop through the string until the end (NULL terminator)
    while(*msg)
    {
        LCD_Char(*msg);     // Call the LCD_Char function multiple times
        msg++;
    }
}

// Get the address of the LCD cursor
char LCD_Address(void)
{
    char temp;
    DATA_DIR = 0xFF;        // Configure the LCD data pins as input
    RS = 0;                 // Select the command register
    RW = 1;                 // Select read mode from LCD to MCU
    EN = 1;                 // High to low pulse on enable pin to read data
    temp = DATA & 0x7F;     // Read the DDRAM address
    Nop();                  // Small delay required to lengthen EN pulse
    Nop();
    EN = 0;                 // Complete the read cycle
    return temp;
}

// Outputs to the LCD
void LCD_Send(const char ch)
{
    RS = 1;             // Select the data register
    RW = 0;             // Select write mode
    EN = 1;             // High to low pulse on enable pin to latch data
    DATA = ch;          // Write to the LCD
    Nop();              // Small delays required to lengthen EN pulse
    Nop();
    Nop();
    EN = 0;             // Complete the write cycle
}

// Clear the LCD display
void LCD_Clear(void)
{
    LCD_Command(0x01);      // Instruction code to clear the display
}

// Clear Second Row and 
//void LCD_Unit_Replace(void)
//{
//    LCD_Command(row2);      // Instruction code to clear the display
//}

// LCD Initial Message
void LCD_Startup(void)
{
    LCD_Clear();        // Clear the LCD display
    LCD_Init();         // Initialise the LCD
    
    LCD_Command(row1);          // First line
    LCD_String(MeterWelcome);   // Display welcome message
    
    LCD_Command(row2);              // Second line
    LCD_String(SelectModeMsg);    // Display mode selection message
}

// Generic setup for LCD Voltage
void LCD_Voltage_Setup(void)
{
    LCD_Clear();        // Clear the LCD display
    LCD_Init();         // Initialise the LCD
    LCD_Command(row1);          // Display the voltage mode header
    LCD_String(VoltageHeader);  // Send the string to the LCD
    
    LCD_Command(row2);              // Display the voltage mode header
    LCD_String(VoltageUnitDisp);    // Send the string to the LCD
   
}


// Generic setup for LCD Resistance
void LCD_Resistance_Setup(void)
{
    LCD_Clear();        // Clear the LCD display
    LCD_Init();         // Initialise the LCD
    LCD_Command(row1);          // Display the voltage mode header
    LCD_String(ResistanceHeader);  // Send the string to the LCD
    
    LCD_Command(row2);              // Display the voltage mode header
    LCD_String(ResistanceUnitDisp);    // Send the string to the LCD
}

// Generic setup for LCD Capacitance
void LCD_Capacitance_Setup(void)
{
    LCD_Clear();        // Clear the LCD display
    LCD_Init();         // Initialise the LCD
    LCD_Command(row1);          // Display the voltage mode header
    LCD_String(CapacitorHeader);  // Send the string to the LCD
    
    LCD_Command(row2);              // Display the voltage mode header
    LCD_String(CapacitorUnitDisp);    // Send the string to the LCD
   
}


// Generic setup for LCD Current
void LCD_Current_Setup(void)
{
    LCD_Clear();        // Clear the LCD display
    LCD_Init();         // Initialise the LCD
    LCD_Command(row1);          // Display the voltage mode header
    LCD_String(CurrentHeader);  // Send the string to the LCD
    
    LCD_Command(row2);              // Display the voltage mode header
    LCD_String(CurrentUnitDisp);    // Send the string to the LCD
   
}


// Generic setup for LCD Current
void LCD_Inductance_Setup(void)
{
    LCD_Clear();        // Clear the LCD display
    LCD_Init();         // Initialise the LCD
    LCD_Command(row1);          // Display the voltage mode header
    LCD_String(InductanceHeader);  // Send the string to the LCD
    
    LCD_Command(row2);              // Display the voltage mode header
    LCD_String(InductanceUnitDisp);    // Send the string to the LCD
   
}

#endif	/* MP_LCD_H */