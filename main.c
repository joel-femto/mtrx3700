/**********************************************************
*  MP_LCD.c
***********************************************************
*  Group 8
***********************************************************
*  This runs the LCD and voltage meter
* 
***********************************************************/

// Include the header files
#include "MP_LCD.h"
#include "VoltageMeter.h"
#include "ResistanceMeter.h"
#include "ModeSelect.h"
#include "CapacitanceMeter.h"
#include "CurrentMeter.h"
#include "InductanceMeter.h"

/***Function declaration*****************************************************/
void globalInterrupt( void );       // Directs to ISR
void ModeSelectIsr(void);           // ISR for mode selection 

/***Global interrupts********************************************************/
#pragma code interruptAddress = 0x0008
void globalInterrupt( void )
{
    _asm goto ModeSelectIsr _endasm
}

/***main*********************************************************************/
#pragma code
void main( void )
{  
    LCD_Startup();
    InitModes();
    
    while(1) 
    {
        Delay1KTCYx(10);
        
        if( currentMode == 0 )
        {
            // ======================== VOLTAGE METER =========================== //
            // Generic LCD Setup for Voltage Mode
            LCD_Voltage_Setup();
            
            while( currentMode == 0 )
            {
 
                // Perform generic ADC setup, clear flags, set channels, and start ADC
                VoltageSetup();

                // --------- Overall Voltage drop across a node --------- //
                voltageDrop = voltage_AN1 - voltage_AN0; //- voltage_AN0;

                // --------- Print this string out to LCD --------- //

                //float n = 233.007;
                ftoa(voltageDrop, voltageReading, 4);

                // Delay for LCD
                Delay1KTCYx(250);       // 250,000 cycles is roughly 

                LCD_Command(row2);              // Display the voltage mode header
                LCD_String(voltageReading);    // Send the string to the LCD    
            }
        }
        
        else if( currentMode == 1 )
        {
            // ======================== RESISTANCE METER =========================== //
            // Generic LCD Setup for Resistance Mode
            LCD_Resistance_Setup();
            
            
            while( currentMode == 1 )
            {
                // Generic ADC setup, channel selection, start ADC and clear interrupt flag
                ResistanceSetup();

                // ----------------- Measured Resistance -------------------------- //
                measured_resistance = (voltage_AN2*R1)/(Vdd-voltage_AN2);
                
                // if (measured_resistance > 100)
                
                    if (measured_resistance > 32000)
                    {
                        LCD_Command(row2);                 // Display the voltage mode header
                        LCD_String(OutsideRangeDisp);    // Send the string to the LCD 
                    }
                    else 
                    {
                        ftoa(measured_resistance, restistanceReading, 2);

                        Delay1KTCYx(250);

                        LCD_Command(row2);                 // Display the voltage mode header
                        LCD_String(restistanceReading);    // Send the string to the LCD
                    }

                // ------------- Print this string out to LCD --------------------- //


            }   
        }
        
        else if( currentMode == 2 )
        {
            // ======================== CAPACITANCE METER =========================== //
            LCD_Capacitance_Setup();
            
            CapacitanceBeginSetup();

            while (capacitorGetAnalogue() < 648) 
            {
                LCD_Command(row2);         // Select Row2
                LCD_String(CapacitanceReading);    // Send the string to the LCD
                Delay1KTCYx(10);
            }

            CapacitanceFinalSetup();


            // ----------------- Measured Capacitance  -------------------------- //
            measured_capacitance = (t_charge*1000000/R2); 

                    if (measured_capacitance < 0.8 || measured_capacitance > 900)
                    {
                        LCD_Command(row2);                 // Display the voltage mode header
                        LCD_String(OutsideRangeDisp);    // Send the string to the LCD   
                    }
                    else
                    {
                        ftoa(measured_capacitance, CapacitanceReading, 4);
                        LCD_Command(row2);                 // Display the voltage mode header
                        LCD_String(CapacitanceReading);    // Send the string to the LCD
                        Delay1KTCYx(10);
                    }
            // ------------- Print this string out to LCD --------------------- //



            // Clear Pins and Discharge Capacitor
            CapacitanceClear();


        }
        else if( currentMode == 3 )
        {
            // ======================== INDUCTANCE METER =========================== //

            LCD_Inductance_Setup();
            CONFIG();                                                                   //Run the configuration function
            
            while( currentMode == 3 ) 
            {

                PORTBbits.RB4 = 1;
                Delay10KTCYx(250);
                
                PORTBbits.RB4 = 0;
                Delay10TCYx(5);
                //CapturePeriod();
        //while(1);
                if(period > 0.0){
                    frequency = 1.0/period;
                    inductance = 1.0/(4.0*3.14159*3.14159*frequency*frequency*capacitance);

                    inductance = inductance * 1e6;
                    
                    if (inductance > 620)
                    {
                        LCD_Command(row2);                 // Display the voltage mode header
                        LCD_String(OutsideRangeDisp);    // Send the string to the LCD   
                    }
                    else
                    {
                        ftoa(inductance, InductanceReading, 4);
                        LCD_Command(row2);                 // Display the voltage mode header
                        LCD_String(InductanceReading);    // Send the string to the LCD
                    }

                    frequency = 1.0;
                    inductance = 1.0;


        }
                
                
                
            }
            
        }
        else if( currentMode == 4 )
        {
            // ======================== CURRENT METER =========================== //
            
            // Generic LCD Setup for Current Mode
            LCD_Current_Setup();
        
             // Perform generic ADC setup 
             ADCSetup();
        
            while( currentMode == 4 ) 
            {
                CurrentSetup();

                // --------- Overall Voltage drop across a node ------------- //
                voltageDrop = voltage_AN3; //- voltage_AN0;

                gain = Rf/Rs;

                current = (voltageDrop/(gain*R_shunt))*10000; // mA

                // SCALING FACTOR FOR SMALL RESISTOR VALUES - cancel out effect of shunt resistor
                if(current > 30) {     // mA
                    //scaling_factor = 1+(R_shunt/(Vdd*1000/current));
                    scaling_factor = 170;
                    current = current + scaling_factor;
                }

                // --------- Print this string out to LCD------------------- //

                ftoa(current, CurrentReading, 3);
                

                LCD_Command(row2);              // Display the voltage mode header
                LCD_String(CurrentReading);    // Send the string to the LCD
                
                Delay10KTCYx(500000);
               
                
                }
        
        }
       
    }
}

/***ISR**********************************************************************/
#pragma interrupt ModeSelectIsr
void ModeSelectIsr( void )
{
    // Confirm that flag is set
    if( INTCONbits.INT0IF )
    {
        INTCONbits.INT0IF = 0;              // Clear interrupt flag
        currentMode = currentMode + 1;      // Increment the current mode
        
        Delay1KTCYx(10);

        // If at the end, go back to beginning
        if( currentMode >= 5 )
        {
            currentMode = 0;
        }
    }
    
}