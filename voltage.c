/* 

Purpose of this script is to generate a 10-bit analog voltage on pins AN0/AN1
The resultant voltage drop across a node will be AN1 - AN0
This voltage will be sent to PutTy via RS-232 cable for DEBUG purposes
This voltage will later be sent to LCD for system integration 

*/

#include <p18f452.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <delays.h>
#include "ConfigRegsPIC18F452.h"

#define Vdd 5.0

unsigned char linefeed[] = "\n\r"; 
unsigned char voltage_line[] = "Voltage reading: ";
unsigned char stringOut[20];

float voltage_AN1;
float voltage_AN0;

float voltageDrop; 

// Reverses a string 'str' of length 'len'
void reverse(char* str, int len) {
    int i = 0, j = len - 1, temp;
    while (i < j) {
        temp = str[i];
        str[i] = str[j];
        str[j] = temp;
        i++;
        j--;
    }
}
  
// Converts a given integer x to string str[]. 
// d is the number of digits required in the output. 
// If d is more than the number of digits in x, 
// then 0s are added at the beginning.
int intToStr(int x, char str[], int d) {
    int i = 0;
    while (x) {
        str[i++] = (x % 10) + '0';
        x = x / 10;
    }
  
    // If number of digits required is more, then
    // add 0s at the beginning
    while (i < d)
        str[i++] = '0';
  
    reverse(str, i);
    str[i] = '\0';
    return i;
}
  

// NEED A FLOAT TO STRING FUNCTION HERE
void ftoa(float n, char* res, int afterpoint) {
    // Extract integer part
    int ipart = (int)n;
  
    // Extract floating part
    float fpart = n - (float)ipart;
  
    // convert integer part to string
    int i = intToStr(ipart, res, 0);
  
    // check for display option after point
    if (afterpoint != 0) {
        res[i] = '.'; // add dot
  
        // Get the value of fraction part upto given no.
        // of points after dot. The third parameter 
        // is needed to handle cases like 233.007
        fpart = fpart * pow(10, afterpoint);
  
        intToStr((int)fpart, res + i + 1, afterpoint);
    }
}

int tx232C_str(unsigned char *txPtr) {
    
    // Check for null terminator 
    while(*txPtr != '\0') {
        
        // Dereference pointer and assign to Transmit register
        TXREG = *txPtr;     
        
        while (TXSTAbits.TRMT == 0) { 
            // Is the serial transmission shift register empty?
        }
        
        txPtr++;     
    }
    
    return 0;
}

// ANALOGUE MODE SETUP HERE 
void ADCSetup(void) {
   
    ADCON0 = 0b01000001; // Fosc/8, turn on ADC module
    ADCON1 = 0b10001110; // Right justify, 1 analogue channel, VDD VSS references 
}

void Timer0Setup(void) {

    T0CON = 0b11000111; //TMR0 prescaler, 1:256, Start Timer
  
}


// USART CONNECTION TO DISPLAY VOLTAGE - FOR DEBUG PURPOSES!!
void SerialSetup(void) {
 
  RCSTAbits.SPEN = 1;   // Serial port enabled
  RCSTAbits.CREN = 1;   // Enable Receive

  SPBRG = 64;           // BAUD RATE - HIGH SPEED and Fosc = 10MHz
  TXSTA = 0b00100100;   
                            /* Master - Doesn't Matter
                            8 Bit Transmit
                            Transmit Enabled            || ENABLED
                            Synchronous Mode            ||Set to ASynch
                            Unimplemented
                            'High baud Rate select'     ||ENABLED
                            'Transmit Shift Register Status Bit'
                            '9th bit of Transmit data' */
 
}


float getVoltage(void) {

    int voltageIn;
    float voltageOut;

    voltageIn = ADRES; // Right-justified ADRESH:ADRESL
        
    voltageOut = (voltageIn*Vdd)/1023.0;
    
    return voltageOut;
  
}

void main(void) {
    

    while(1) {
                
        Delay1KTCYx(4);

        // Perform generic ADC setup 
        ADCSetup();

        // Clear ADC interrupt flag
        PIR1bits.ADIF = 0; 

        // Select Channel 1 for V+ve
        ADCON0bits.CHS0 = 1; 

        // Wait acquisition time
        Delay1KTCYx(4);
        
        // Start ADC 
        ADCON0bits.GO = 1;

        while(PIR1bits.ADIF == 0);

        // Fetch voltage at AN1
        voltage_AN1 = getVoltage();

        // Clear ADC interrupt flag
        PIR1bits.ADIF = 0; 
        
         // Select Channel 0 for V-ve
        ADCON0bits.CHS0 = 0; 

        // Wait acquisition time
        Delay1KTCYx(4);

        // Start ADC
        ADCON0bits.GO = 1;

        while(PIR1bits.ADIF == 0);

        voltage_AN0 = getVoltage();

        // Clear ADC interrupt flag
        PIR1bits.ADIF = 0;   

         
        // --------- Overall Voltage drop across a node --------- //
        voltageDrop = voltage_AN1 - voltage_AN0; //- voltage_AN0;


        // --------- Print this string out to LCD/PutTy via USART --------- //

        SerialSetup();

        //float n = 233.007;
        ftoa(voltageDrop, stringOut, 4);

        // Transmit string to PutTy via USART
        tx232C_str(voltage_line);
        tx232C_str(stringOut);
        tx232C_str(linefeed); 

        }

}
  
  
      
 