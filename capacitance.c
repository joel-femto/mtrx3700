/* 

Purpose of this script is to generate a 10-bit analog voltage on pins AN0/AN1
The resultant voltage drop across a node will be AN1 - AN0
This voltage will be sent to PutTy via RS-232 cable for DEBUG purposes
This voltage will later be sent to LCD for system integration 

*/

#include <p18f452.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <delays.h>
#include "ConfigRegsPIC18F452.h"

#define Vdd 5.0
#define R1 9000.0
#define instruct_cycle_time 0.4e-6; // 10MHz

unsigned char linefeed[] = "\n\r"; 
unsigned char capacitance_line[] = "Capacitance reading: ";

int timer0;
unsigned char stringOut[20];

float voltage_AN4;
float measured_capacitance;
float t_charge; 

// Reverses a string 'str' of length 'len'
void capacitorReverse(char* str, int len) {
    int i = 0, j = len - 1, temp;
    while (i < j) {
        temp = str[i];
        str[i] = str[j];
        str[j] = temp;
        i++;
        j--;
    }
}
  
// Converts a given integer x to string str[]. 
// d is the number of digits required in the output. 
// If d is more than the number of digits in x, 
// then 0s are added at the beginning.
int capacitorIntToStr(int x, char str[], int d) {
    int i = 0;
    while (x) {
        str[i++] = (x % 10) + '0';
        x = x / 10;
    }
  
    // If number of digits required is more, then
    // add 0s at the beginning
    while (i < d)
        str[i++] = '0';
  
    capacitorReverse(str, i);
    str[i] = '\0';
    return i;
}
  

// NEED A FLOAT TO STRING FUNCTION HERE
void capacitorFtoa(float n, char* res, int afterpoint) {
    // Extract integer part
    int ipart = (int)n;
  
    // Extract floating part
    float fpart = n - (float)ipart;
  
    // convert integer part to string
    int i = capacitorIntToStr(ipart, res, 0);
  
    // check for display option after point
    if (afterpoint != 0) {
        res[i] = '.'; // add dot
  
        // Get the value of fraction part upto given no.
        // of points after dot. The third parameter 
        // is needed to handle cases like 233.007
        fpart = fpart * pow(10, afterpoint);
  
        capacitorIntToStr((int)fpart, res + i + 1, afterpoint);
    }
}

int tx232C_str(unsigned char *txPtr){
    
    // Check for null terminator 
    while(*txPtr != '\0'){
        
        // Dereference pointer and assign to Transmit register
        TXREG = *txPtr;     
        
        while (TXSTAbits.TRMT == 0){ 
            // Is the serial transmission shift register empty?
        }
        
        txPtr++;     
    }
    
    return 0;
}

// ANALOGUE MODE SETUP HERE 
void ADCSetup(void) {
   
    ADCON0 = 0b01000001; // Fosc/8, turn on ADC module
    ADCON1 = 0b10001110; // Right justify, 1 analogue channel, VDD VSS references 
}

void Timer0Setup(void) {

    T0CON = 0b00000110; //TMR0 prescaler, 1:256, NOT-ON YET!
  
}


// USART CONNECTION TO DISPLAY VOLTAGE - FOR DEBUG PURPOSES!!
void SerialSetup(void){
 
  RCSTAbits.SPEN = 1;   // Serial port enabled
  RCSTAbits.CREN = 1;   // Enable Receive

  SPBRG = 64;           // BAUD RATE - HIGH SPEED and Fosc = 10MHz
  TXSTA = 0b00100100;   
                            /* Master - Doesn't Matter
                            8 Bit Transmit
                            Transmit Enabled            || ENABLED
                            Synchronous Mode            ||Set to ASynch
                            Unimplemented
                            'High baud Rate select'     ||ENABLED
                            'Transmit Shift Register Status Bit'
                            '9th bit of Transmit data' */
 
}


float CapacitorGetVoltage(void) {

    int voltageIn;
    float voltageOut;

    voltageIn = ADRES; // Right-justified ADRESH:ADRESL
        
    voltageOut = (voltageIn*Vdd)/1023.0;
    
    return voltageOut;
  
}

int capacitorGetAnalogue(void) {
    // Clear ADC interrupt flag
        PIR1bits.ADIF = 0; 

        // Select Channel 1 for V+ve
        //ADCON0bits.CHS0 = 1; 

        // Select Channel 4 for Voltage_divider V
        ADCON0bits.CHS0 = 0; 
        ADCON0bits.CHS1 = 0; 
        ADCON0bits.CHS2 = 1; 
      
        // Wait acquisition time
        Delay1KTCYx(4);
        
        // Start ADC 
        ADCON0bits.GO = 1;

        while(PIR1bits.ADIF == 0);

        // Fetch voltage at AN1
        //voltage_AN1 = getVoltage();
        
        // Fetch voltage at AN4
        //analogue_out = ADRESvoltage_AN4 = getVoltage();

        // Clear ADC interrupt flag
        PIR1bits.ADIF = 0; 
        
        return ADRES;
    
}

void main(void) {
    
    while(1) {
        
        Timer0Setup();
        
        TMR0H = 0;
        TMR0L = 0;
        timer0 = 0;
                
        //Delay1KTCYx(4);

        // Perform generic ADC setup 
        ADCSetup();

        capacitorGetAnalogue(); 
           
        // --------- Overall Voltage drop across a node --------- //
        // Wait till voltage drop across capacitor is 63.3% of Vdd
        
        TRISCbits.RC0 = 0; // Charge resistor pin 10K output
        PORTCbits.RC0 = 0; // Low voltage init
            
        
        PORTCbits.RC0 = 1; // Toggle charge pin
        
        T0CONbits.TMR0ON = 1; // Turn on TIMER0
    
        while (capacitorGetAnalogue() < 648) {
        }
           
        T0CONbits.TMR0ON = 0; // Stop TIMER0

        // Define Timer 
        timer0 = TMR0L;
        timer0 = timer0 + 256*TMR0H;
        //timer0 = timer0 + TMR0L;
            
        // Convert to seconds
        t_charge = (0.4e-6)*(128)*(timer0);
          
        
        measured_capacitance = (t_charge*1000000/R1); 
                           
        SerialSetup();

        //float n = 233.007;
        //ftoa(voltageDrop, stringOut, 4);
        capacitorFtoa(measured_capacitance, stringOut, 4);

        // Transmit string to PutTy via USART
        //tx232C_str(voltage_line);
        tx232C_str(capacitance_line);
        tx232C_str(stringOut);
        tx232C_str(linefeed); 
    
        PORTCbits.RC0 = 0; // Clear charge pin
        TRISCbits.RC1 = 0; // Discharge pin output
        PORTCbits.RC1 = 0; // Discharge pin low
        // Wait while capacitor discharges
        
        while (capacitorGetAnalogue() > 0) {
        }
        
        TRISCbits.RC1 = 1; // Discharge pin input ==> High impedance

    }
    
    
        
    

        
        
        //measured_resistance = (voltage_AN2*R1)/(Vdd-voltage_AN2);

        // --------- Print this string out to LCD/PutTy via USART --------- //



}


  
  
      
 