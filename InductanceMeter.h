/* 

Purpose of this script is to generate a 10-bit analog voltage on pins AN0/AN1
The resultant voltage drop across a node will be AN1 - AN0
This voltage will be sent to PutTy via RS-232 cable for DEBUG purposes
This voltage will later be sent to LCD for system integration 

*/
#ifndef INDUCTANCEMETER_H
#define	INDUCTANCEMETER_H


#include <p18f452.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <delays.h>
#include "ConfigRegsPIC18F452.h"

unsigned char linefeed[] = "\n\r";
unsigned char inductance_line[] = "Inductance reading: ";
unsigned char InductanceReading[20];

int count;

void CONFIG(void);
void CapturePeriod(void);

float period, frequency, inductance;

float capacitance = 2.2e-6;   


void Timer0Setup(void) {

    T0CON = 0b11000111; //TMR0 prescaler, 1:256, Start Timer

}


void CONFIG(void){
    INTCONbits.GIEL = 0;
    INTCONbits.GIEH = 0;
    CCP1CON = 0B00000101;
    T3CON = 0B00000000;
    T1CON = 0B00000000;
    RCONbits.IPEN = 1;   // enable priorities
    IPR1bits.CCP1IP = 0; // high priority
    PIE1bits.CCP1IE = 1;
    PIR1bits.CCP1IF = 0;
    TRISCbits.TRISC2 = 1;
    TRISBbits.TRISB4 = 0;
    PORTBbits.RB4 = 0;
    INTCONbits.GIEL = 1;
    INTCONbits.GIEH = 1;
}

#pragma interruptlow lowPriorityISR
void lowPriorityISR(void){


    if (PIR1bits.CCP1IF) {
        
        if (count == 0) {
            period = 0.0;
            TMR1H = 0;
            TMR1L = 0;
             // Start Timer1
            T1CONbits.TMR1ON = 1;
        }
        
        PIR1bits.CCP1IF = 0;

        count = count + 1;

        if (count == 2) {
            T1CONbits.TMR1ON = 0;
            period = TMR1L;
            period = period + TMR1H*256;
            period = period * 0.4e-6;   
        }
        if (count > 2) {
            count = 0; 
        }

    }
    
}


// Memory Location for high Priority Interrupts 
#pragma code lowPriorityAddress = 0x18
void lowPriority(void){
 _asm
 goto lowPriorityISR _endasm
}



#endif


