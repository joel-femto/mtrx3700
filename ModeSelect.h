/* 
 * File:   ModeSelect.h
 * Author: Group 8
 *
 * Created on 18 October 2021, 9:36 PM
 * 
 * For selecting the modes of operation - total 5 for A, V, R, C, L
 */

#ifndef MODESELECT_H
#define	MODESELECT_H

/***Libraries****************************************************************/
#include "ConfigRegsPIC18f452.h"    // Configure registers
#include "p18f452.h"                // Wrapper for PIC18
//#include <p18cxxx.h>               // wrapper for all PIC18 headers
#include <stdio.h>
#include <stdlib.h>

/***Pin defines**************************************************************/
#define MODE_SELECT PORTBbits.RB0           // PORTB<0> for Mode Select
#define MODE_SELECT_DIR TRISBbits.TRISB0    // Mode select direction register

/***Function declaration*****************************************************/
void InitModes(void);               // Initialiser

/***Global variables*********************************************************/
unsigned char currentMode = 6;      // Initially 6 so it doesn't go into any if statements

/***Functions****************************************************************/

// Initialise the mode selection 
void InitModes(void)
{
    MODE_SELECT = 0xFF;         // Clear the pin
    MODE_SELECT_DIR = 0xFF;     // Set the pin as input
    
//    INTCONbits.GIE = 1;         // Enable unmasked interrupts
//    INTCONbits.INT0IE = 1;      // Enable external interrupt
//    INTCONbits.INT0IF = 0;      // Clear interrupt flag
    
    INTCONbits.GIEH = 1;
    INTCONbits.GIEL = 1;
    INTCON2bits.INTEDG0 = 1;
    
    INTCONbits.INT0IE = 1;      // Enable external interrupt
    INTCONbits.INT0IF = 0;      // Clear interrupt flag
    
    
}

#endif