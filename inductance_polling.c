//Major Project - Inductance Meter
//Group 8

#include <p18f452.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <delays.h>
#include "ConfigRegsPIC18F452.h"


unsigned char linefeed[] = "\n\r";
unsigned char inductance_line[] = "Inductance reading: ";
unsigned char stringOut[20];

void CONFIG(void);
void CapturePeriod(void);

float period, frequency, inductance, capacitance;


// Reverses a string 'str' of length 'len'
void reverse(char* str, int len) {
    int i = 0, j = len - 1, temp;
    while (i < j) {
        temp = str[i];
        str[i] = str[j];
        str[j] = temp;
        i++;
        j--;
    }
}

// Converts a given integer x to string str[].
// d is the number of digits required in the output.
// If d is more than the number of digits in x,
// then 0s are added at the beginning.
int intToStr(int x, char str[], int d) {
    int i = 0;
    while (x) {
        str[i++] = (x % 10) + '0';
        x = x / 10;
    }

    // If number of digits required is more, then
    // add 0s at the beginning
    while (i < d)
        str[i++] = '0';

    reverse(str, i);
    str[i] = '\0';
    return i;
}


// NEED A FLOAT TO STRING FUNCTION HERE
void ftoa(float n, char* res, int afterpoint) {
    // Extract integer part
    int ipart = (int)n;

    // Extract floating part
    float fpart = n - (float)ipart;

    // convert integer part to string
    int i = intToStr(ipart, res, 0);

    // check for display option after point
    if (afterpoint != 0) {
        res[i] = '.'; // add dot

        // Get the value of fraction part upto given no.
        // of points after dot. The third parameter
        // is needed to handle cases like 233.007
        fpart = fpart * pow(10, afterpoint);

        intToStr((int)fpart, res + i + 1, afterpoint);
    }
}

int tx232C_str(unsigned char *txPtr) {

    // Check for null terminator
    while(*txPtr != '\0') {

        // Dereference pointer and assign to Transmit register
        TXREG = *txPtr;

        while (TXSTAbits.TRMT == 0) {
            // Is the serial transmission shift register empty?
        }

        txPtr++;
    }

    return 0;
}

void Timer0Setup(void) {

    T0CON = 0b11000111; //TMR0 prescaler, 1:256, Start Timer

}

// USART CONNECTION TO DISPLAY VOLTAGE - FOR DEBUG PURPOSES!!
void SerialSetup(void) {

  RCSTAbits.SPEN = 1;   // Serial port enabled
  RCSTAbits.CREN = 1;   // Enable Receive

  SPBRG = 64;           // BAUD RATE - HIGH SPEED and Fosc = 10MHz
  TXSTA = 0b00100100;
                            /* Master - Doesn't Matter
                            8 Bit Transmit
                            Transmit Enabled            || ENABLED
                            Synchronous Mode            ||Set to ASynch
                            Unimplemented
                            'High baud Rate select'     ||ENABLED
                            'Transmit Shift Register Status Bit'
                            '9th bit of Transmit data' */

}

void CONFIG(void){
    CCP1CON = 0B00000101;
    T3CON = 0B00000000;
    T1CON = 0B00000000;
    PIE1bits.CCP1IE = 1;
    PIR1bits.CCP1IF = 0;
    TRISCbits.TRISC2 = 1;
    TRISBbits.TRISB4 = 0;
    PORTBbits.RB4 = 0;
}

void CapturePeriod(void){
    period = 0.0;
    TMR1H = 0;
    TMR1L = 0;
    PIR1bits.CCP1IF = 0;
    while(PIR1bits.CCP1IF == 0);
    T1CONbits.TMR1ON = 1;
    PIR1bits.CCP1IF = 0;
    while(PIR1bits.CCP1IF == 0);
    T1CONbits.TMR1ON = 0;
    period = TMR1L;
    period = period + TMR1H*256;
    period = period * 0.4e-6;
}

void main(void) {

    SerialSetup();

    CONFIG();                                                                   //Run the configuration function

    capacitance = 2.7e-6; 

    while(1){
        PORTBbits.RB4 = 1;
        Delay10KTCYx(250);
        PORTBbits.RB4 = 0;
        Delay10TCYx(5);
        CapturePeriod();
        if(period > 0.0){
            frequency = 1.0/period;
            inductance = 1.0/(4.0*3.14159*3.14159*frequency*frequency*capacitance);

            inductance = inductance * 1e6;
            ftoa(inductance, stringOut, 4);
            // Transmit string to PutTy via USART
            tx232C_str(inductance_line);
            tx232C_str(stringOut);
            tx232C_str(linefeed);
            frequency = 1.0;
            inductance = 1.0;

        }
    }

}
