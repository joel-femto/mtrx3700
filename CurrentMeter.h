/* 

Purpose of this script is to generate a 10-bit analog voltage on pins AN0/AN1
The resultant voltage drop across a node will be AN1 - AN0
This voltage will be sent to PutTy via RS-232 cable for DEBUG purposes
This voltage will later be sent to LCD for system integration 

*/
#ifndef CURRENTMETER_H
#define	CURRENTMETER_H


#include <p18f452.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <delays.h>
#include "ConfigRegsPIC18F452.h"

#define Vdd 5.0
#define Rf 16000
#define Rs 1000
#define R_shunt 6.5


unsigned char CurrentReading[20];

int gain;

float scaling_factor;

float current;

float voltage_AN3;

float voltageDrop;


// Current Functions 
void CurrentSetup(void);



void CurrentSetup(void)
{
                Delay1KTCYx(4);

                // Clear ADC interrupt flag
                PIR1bits.ADIF = 0; 

                // Select Channel 3 for V+ve
                ADCON0bits.CHS0 = 1;
                ADCON0bits.CHS1 = 1;
                ADCON0bits.CHS2 = 0;

                // Wait acquisition time
                Delay1KTCYx(4);

                // Start ADC 
                ADCON0bits.GO = 1;

                while(PIR1bits.ADIF == 0);

                // Fetch voltage at AN1
                voltage_AN3 = getVoltage();

                // Clear ADC interrupt flag
                PIR1bits.ADIF = 0; 
    
}


#endif


